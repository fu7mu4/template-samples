# template-samplesについて / About this project

このリポジトリはIssue templateと MR templateを試すために作成しています。
開発者やリポジトリのオーナーは試してよければ自身のプロジェクトに採用してくだちい。

This repository describe Gitlab Issue and MR's template.
You can add any templates for you freely to your project.

# 使い方 How to use this

## あなたのプロジェクトに適用 How to use these template files for your project

- 課題のテンプレートは、`.md`の拡張子のファイルで作成し、デフォルトブランチの `.gitlab/issue_templates` にpush
- MRのテンプレートは、`.md`の拡張子のファイルで作成し、デフォルトブランチの `.gitlab/merge_request_templates` にpush
- テンプレートは複数登録できます。
- デフォルトブランチは通常 master (or main)


- push new Issue template files with `.md` suffix into `.gitlab/issue_templates` of your default branch
- push new MR template files with `.md` suffix into `.gitlab/merge_request_templates` of your default branch
- default brach name: master (or main)

## 試すまたは修正 How to try to use or modify these templates files

1. このプロジェクトをフォークして、gitlab上のあなたの名前空間にコピーしてください。
2. その後、試して下さい。
    1. 課題を追加
    2. マージリクエストを追加
    3. テンプレートを編集

1. Please fork this project to copy this to your group(namespace) on this gitlab.
2. Try bellow things to do.
    1. Add new Issues on your project
    2. Add new Merge Requests to your project
    3. Modify your templates files for you

### フォーク fork this project

ページ上部の「フォーク」を押下してこのプロジェクトをフォークしてください。　push the "Fork" button on top of this page to fork this projec
![image.png](./image.png)

### 課題の作成 Add new Issues on your project

### マージリクエストを作成 Add new Merge Request to your project

### テンプレートを編集 Modify your templates files for you


# Issue と MR

課題とマージリクエストは Gitlab風のマークダウンです。
You can write any issue and merge request comment in Gitlab flavor Markdown

## Gitlab 風/ Gitlab flavor

いくつかの特別で便利なリンクの書き方を紹介します。
We describe some useful special link

| Syntax | 説明 | Description |
|:------:|:------:|:---------:|
| `#1111`  | 番号1111のIssueへのリンク| link to issue that has the number 1111 |
| `!1111`  | 番号1111のMRへのリンク   | link to Marge Request that has the number 1111|
| `&1111`  | 番号1111のSnippetへのリンク| link to Snippet that has the number 1111|
| `[name](URL)`  | URLへのリンク、nameは表示| link to the URL with name |



# ラベル/ Label

ラベルは、課題やMRを区別するためのものです。
Label helps you to distinguich type of issues and merge request. 

GitLabの課題→ラベルを参照してください。
Please visit your Gitlab Project and go to Issues then go labels

# クイックアクション / quick actions 

GitLab は slackの スラッシュコマンドに似たクイックアクションをサポートしています。これらを書き込むとすぐに実行され消えます。
Gitlab has quick action liks a slash command in slack. As typing the commands, gitlab run several action.

|Command|アクション|
|:------|:-----|
|`/close`|issue または merge request をclose|
|`/reopen`|issue または merge request をReopen|
|`/title <New title>`|タイトルを変更|
|`/assign @username`|Assignするレビュアー割り当て|
|`/unassign`|Assignしない、レビュアー取り消し|
|`/milestone %milestone`|milestoneの設定|
|`/remove_milestone`|milestoneを外す|
|`/label ~foo ~"bar baz"`|label(s)の追加|
|`/unlabel ~foo ~"bar baz"`|すべてまたは指定ラベルの削除|
|`/relabel ~foo ~"bar baz"`|ラベルの置換|
|`/todo`|todoに追加|
|`/done`|todoをdoneへ|
|`/subscribe`|Subscribe :thumbsup:|
|`/unsubscribe`| Unsubscribe :thumbsdown:|
|`/due <in 2 days | this Friday | December 31st>`| due dateを設定|
|`/remove_due_date`| due dateを外す|
|`/wip`| Work In Progress作業中の切り替え|


[gitlabのクイックアクション一覧(英語)](https://gitlab.com/help/user/project/quick_actions.md)
[Quick Action in Gitlab](https://gitlab.com/help/user/project/quick_actions.md)

# 公式情報

[gitlabのドキュメントページ(英語)](https://about.gitlab.com/blog/2016/03/08/gitlab-tutorial-its-all-connected/)
[gitlab tutorial](https://about.gitlab.com/blog/2016/03/08/gitlab-tutorial-its-all-connected/)



