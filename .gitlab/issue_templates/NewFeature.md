# 要約/Summary

(新機能の目的やまとめを書く/summarizing new feature you designed or goal)

# チケット番号/Ticket

(# for Issue, ! for MR, $ for Snippet, [Jira Ticket Link](https://hrnd-ivipoc-jira.micdev.jp/browse/SCMTLDW-XXX) for jira)

# 機能詳細/Description

(提案する操作手順を書いてください。/Describe details of your feature )
1. 
2. 
3. 

# 期待した振る舞い/Expected Behavior

(新しい仕様として正しい動作を書いてください/What is expected/right behavior?)

/label ~suggestion

