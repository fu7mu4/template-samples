# 要約/Summary

(バグのまとめを書く/summarizing the bug you encounted)

# チケット番号/Ticket

(# for Issue, ! for MR, $ for Snippet, [Jira Ticket Link](https://hrnd-ivipoc-jira.micdev.jp/browse/SCMTLDW-XXX) for jira)

# 再現手順/Steps to reproduce

(再現手順を書いてください。とても重要です。/How one can reproduce the issue - this is very important !!)
1. さいしょに/First ....
2. つぎに/Second ......
3. そんつぎに/Third .....

# 期待した振る舞い/Expected Behavior

(仕様として正しい動作を書いてください/What is expected/right behavior?)

# 実際の動作/Actually Result

(実際に起きたことを書いてください/What actually happens)

# 証拠またはログ/Related log and/or screenshot

(ログやスクショをお願いします/Paste related log or screenshot  )

# 修正方法の提案/Possible fix

(修正方法の提案があればお願い/If you have ideas, please share that)

/label ~bug
