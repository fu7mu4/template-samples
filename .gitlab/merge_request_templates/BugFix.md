# 要約/Summary

(バグの修正のまとめを書く/Summarized the fixing bug )

# チケット番号/Ticket

(# for Issue, ! for MR, $ for Snippet, [Jira Ticket Link](https://hrnd-ivipoc-jira.micdev.jp/browse/SCMTLDW-XXX) for jira)

# 影響範囲/Bug Scope

(バグの影響範囲を書く/Identified Scope of bug)

# 原因/Why, Identified Reason

(バグの原因や真因を書く/Why the bug happens)

# 修正方法/How to fix/avoid the bug

(MRの承認者がわかるようにバグの修正の詳細を書く/Describe your fix/avoidance clearly to make the assignee understand)

# 修正結果の証拠/Proof of your fix

(修正できたことを示すログまたはスクリーンショットをペーストしてください/paste log or screenshot to prove your fix)

/label ~bug
